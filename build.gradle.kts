plugins {
    kotlin("jvm") version "1.3.31"
    kotlin("plugin.jpa") version "1.3.31"
    kotlin("plugin.spring") version "1.3.31"
    id("io.spring.dependency-management") version "1.0.7.RELEASE"
    id("org.springframework.boot") version "2.1.5.RELEASE"
    id("org.jmailen.kotlinter") version "1.25.2"
    id("com.adarshr.test-logger") version "1.6.0"
    id("jacoco")
}

group = "br.com.beblue"
version = "1.0"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.valiktor:valiktor-core:0.5.0")
    implementation("org.valiktor:valiktor-javatime:0.5.0")
    implementation("org.valiktor:valiktor-javamoney:0.5.0")
    implementation("org.valiktor:valiktor-spring-boot-starter:0.5.0")
    implementation("javax.money:money-api:1.0.3")
    implementation("org.zalando:jackson-datatype-money:1.1.1")
    implementation("org.jadira.usertype:usertype.core:7.0.0.CR1")
    implementation("br.com.caelum.stella:caelum-stella-core:2.1.3")
    implementation("io.springfox:springfox-swagger2:2.9.2")
    implementation("io.springfox:springfox-swagger-ui:2.9.2")

    runtimeOnly("com.h2database:h2")
    runtimeOnly("org.flywaydb:flyway-core")
    runtimeOnly("org.javamoney:moneta:1.3")

    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

configurations.all {
    exclude(group = "junit")
    exclude(group = "org.mockito")
}

repositories {
    mavenCentral()
}

testlogger {
    setTheme("mocha")
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
        }
    }

    compileTestKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
        }
    }

    test {
        useJUnitPlatform()
    }

    bootJar {
        archiveFileName.set("app.jar")
    }

    jacocoTestReport {
        reports {
            xml.isEnabled = true
            html.isEnabled = true
        }
    }

    jacocoTestCoverageVerification {
        dependsOn(jacocoTestReport)

        violationRules {
            rule { limit { minimum = 0.0.toBigDecimal() } }
        }
    }

    check {
        dependsOn(jacocoTestCoverageVerification)
    }
}