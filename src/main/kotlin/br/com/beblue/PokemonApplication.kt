package br.com.beblue

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@EnableCaching
@SpringBootApplication
class PokemonApplication

fun main(args: Array<String>) {
    runApplication<PokemonApplication>(*args)
}