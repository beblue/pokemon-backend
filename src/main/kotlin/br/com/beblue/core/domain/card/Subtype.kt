package br.com.beblue.core.domain.card

enum class Subtype(private val subtype: String) {

    BASIC("Basic"),
    STAGE_1("Stage 1"),
    STAGE_2("Stage 2");

    override fun toString() = subtype

    companion object {
        fun fromString(subtype: String) = Subtype.values().firstOrNull { it.subtype == subtype }
    }
}