package br.com.beblue.core.domain

import br.com.caelum.stella.validation.CPFValidator
import br.com.caelum.stella.validation.InvalidStateException
import org.valiktor.Constraint
import org.valiktor.Validator

object CPF : Constraint

fun <T> Validator<T>.Property<String?>.isCPF() =
    this.validate(CPF) {
        it == null ||
            try {
                CPFValidator(false).assertValid(it)
                true
            } catch (ex: InvalidStateException) {
                false
            }
    }