package br.com.beblue.core.domain.card

data class Ability(
    val name: String,
    val text: String
)