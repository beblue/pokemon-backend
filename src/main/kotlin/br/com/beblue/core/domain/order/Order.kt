package br.com.beblue.core.domain.order

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.player.PlayerId
import br.com.beblue.core.domain.toMoney
import org.valiktor.functions.isNotEmpty
import org.valiktor.validate
import java.time.OffsetDateTime
import java.util.UUID
import javax.money.Monetary
import javax.money.MonetaryAmount

typealias OrderId = UUID

data class InsufficientBalanceException(val amount: MonetaryAmount, val availableAmount: MonetaryAmount) : Exception()

data class Order(
    val orderId: OrderId = OrderId.randomUUID(),
    val playerId: PlayerId,
    val status: OrderStatus = OrderStatus.PENDING,
    val items: Set<Item>
) {
    val occurredOn: OffsetDateTime = OffsetDateTime.now()
    val totalAmount: MonetaryAmount = items.map(Item::amount).fold(0.toMoney(BRL), MonetaryAmount::add)
    val totalPaidAmount: MonetaryAmount = items.map(Item::paidAmount).fold(0.toMoney(BRL), MonetaryAmount::add)
    val totalBurnedCashbackAmount: MonetaryAmount = items.map(Item::burnedCashbackAmount).fold(0.toMoney(BRL), MonetaryAmount::add)
    val totalEarnedCashbackAmount: MonetaryAmount = items.map(Item::earnedCashbackAmount).fold(0.toMoney(BRL), MonetaryAmount::add)

    init {
        validate(this) {
            validate(Order::items).isNotEmpty()
        }
    }

    fun confirmPayment(availableAmount: MonetaryAmount, availableCashbackAmount: MonetaryAmount): Order {
        val totalAvailableAmount = availableAmount.add(availableCashbackAmount)

        if (this.totalAmount > totalAvailableAmount) {
            throw InsufficientBalanceException(totalAmount, totalAvailableAmount)
        }

        var remainingAvailableAmount = availableAmount
        var remainingAvailableCashbackAmount = availableCashbackAmount

        return this.copy(
            status = OrderStatus.CONFIRMED,
            items = this.items.map { item ->
                var remainingAmount = item.amount

                val paidAmount = if (remainingAvailableAmount >= remainingAmount) remainingAmount else remainingAvailableAmount
                remainingAmount = remainingAmount.subtract(paidAmount)
                remainingAvailableAmount = remainingAvailableAmount.subtract(paidAmount)

                val burnedCashbackAmount = if (remainingAmount.isPositive) remainingAmount else 0.toMoney(BRL)
                remainingAvailableCashbackAmount = remainingAvailableCashbackAmount.subtract(burnedCashbackAmount)

                val earnedCashbackAmount = if (paidAmount.isPositive)
                    paidAmount.multiply(item.cashbackPercentage).divide(100).with(Monetary.getDefaultRounding())
                else
                    0.toMoney(BRL)

                item.copy(
                    paidAmount = paidAmount,
                    burnedCashbackAmount = burnedCashbackAmount,
                    earnedCashbackAmount = earnedCashbackAmount
                )
            }.toSet()
        )
    }
}