package br.com.beblue.core.domain.order

import br.com.beblue.core.domain.player.PlayerId

interface OrderRepository {

    fun findByPlayerId(playerId: PlayerId): Collection<Order>

    fun findByOrderId(orderId: OrderId): Order?

    fun create(order: Order)
}