package br.com.beblue.core.domain.card

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.toMoney
import java.net.URI
import javax.money.MonetaryAmount

typealias CardId = String

data class Card(
    val cardId: CardId,
    val name: String,
    val number: Int,
    val hp: Int,
    val rarity: Rarity,
    val type: Type,
    val subtype: Subtype,
    val evolvesFrom: String?,
    val ability: Ability?,
    val attacks: Set<Attack>,
    val resistances: Map<Type, String>,
    val weaknesses: Map<Type, String>,
    val smallImageUri: URI,
    val largeImageUri: URI
) {
    val amount: MonetaryAmount = calculateAmount()
    val cashbackPercentage: Int = calculateCashbackPercentage()

    private fun calculateAmount(): MonetaryAmount {
        // TODO
        return 0.toMoney(BRL)
    }

    private fun calculateCashbackPercentage(): Int {
        // TODO
        return 0
    }
}