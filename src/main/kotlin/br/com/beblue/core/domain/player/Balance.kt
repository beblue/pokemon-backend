package br.com.beblue.core.domain.player

import javax.money.MonetaryAmount

data class Balance(
    val availableAmount: MonetaryAmount,
    val availableCashbackAmount: MonetaryAmount
)