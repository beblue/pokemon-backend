package br.com.beblue.core.domain

import javax.money.CurrencyUnit
import javax.money.Monetary
import javax.money.MonetaryAmount

val BRL: CurrencyUnit = Monetary.getCurrency("BRL")

fun <T : Number> T.toMoney(currency: CurrencyUnit): MonetaryAmount =
    Monetary.getDefaultAmountFactory().setNumber(this).setCurrency(currency).create()