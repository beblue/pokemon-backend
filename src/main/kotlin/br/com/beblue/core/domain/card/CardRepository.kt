package br.com.beblue.core.domain.card

interface CardRepository {

    fun find(name: Set<String>, type: Set<Type>, rarity: Set<Rarity>): Collection<Card>

    fun findByCardId(cardId: CardId): Card?
}