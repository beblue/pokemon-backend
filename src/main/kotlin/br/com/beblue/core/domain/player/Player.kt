package br.com.beblue.core.domain.player

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.isCPF
import br.com.beblue.core.domain.toMoney
import org.valiktor.functions.hasSize
import org.valiktor.functions.isNotBlank
import org.valiktor.validate
import javax.money.MonetaryAmount

typealias PlayerId = String

data class PlayerAlreadyExistsException(val playerId: PlayerId) : Exception()

data class Player(
    val playerId: PlayerId,
    val name: String,
    val balance: Balance = Balance(
        availableAmount = 100.toMoney(BRL),
        availableCashbackAmount = 0.toMoney(BRL)
    ),
    val deck: Set<CardId> = emptySet()
) {

    init {
        validate(this) {
            validate(Player::playerId).isNotBlank().isCPF()
            validate(Player::name).isNotBlank().hasSize(max = 50)
        }
    }

    fun updateBalance(
        paidAmount: MonetaryAmount,
        burnedCashbackAmount: MonetaryAmount,
        earnedCashbackAmount: MonetaryAmount
    ) = this.copy(
        balance = Balance(
            availableAmount = this.balance.availableAmount.subtract(paidAmount),
            availableCashbackAmount = this.balance.availableCashbackAmount
                .subtract(burnedCashbackAmount)
                .add(earnedCashbackAmount)
        )
    )

    fun addCards(cards: Set<CardId>) = this.copy(deck = deck + cards)
}