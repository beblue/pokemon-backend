package br.com.beblue.core.domain.order

enum class OrderStatus {

    PENDING,
    CONFIRMED
}