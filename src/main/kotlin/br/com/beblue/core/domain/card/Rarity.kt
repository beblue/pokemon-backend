package br.com.beblue.core.domain.card

enum class Rarity(private val rarity: String) {

    COMMON("Common"),
    UNCOMMON("Uncommon"),
    RARE("Rare");

    override fun toString() = rarity

    companion object {
        fun fromString(rarity: String) = Rarity.values().firstOrNull { it.rarity == rarity }
    }
}