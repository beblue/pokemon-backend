package br.com.beblue.core.domain.player

interface PlayerRepository {

    fun findByPlayerId(playerId: PlayerId): Player?

    fun create(player: Player)

    fun update(player: Player)
}