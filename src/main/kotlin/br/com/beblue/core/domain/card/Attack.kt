package br.com.beblue.core.domain.card

data class Attack(
    val name: String,
    val text: String,
    val damage: String,
    val cost: Set<Type>
)