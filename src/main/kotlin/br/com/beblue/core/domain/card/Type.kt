package br.com.beblue.core.domain.card

enum class Type(private val type: String) {

    FIRE("Fire"),
    WATER("Water"),
    GRASS("Grass"),
    PSYCHIC("Psychic"),
    COLORLESS("Colorless"),
    FIGHTING("Fighting"),
    LIGHTNING("Lightning");

    override fun toString() = type

    companion object {
        fun fromString(type: String) = Type.values().firstOrNull { it.type == type }
    }
}