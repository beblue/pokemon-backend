package br.com.beblue.core.domain.order

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.toMoney
import javax.money.MonetaryAmount

data class Item(
    val cardId: CardId,
    val cashbackPercentage: Int,
    val amount: MonetaryAmount,
    val paidAmount: MonetaryAmount = 0.toMoney(BRL),
    val burnedCashbackAmount: MonetaryAmount = 0.toMoney(BRL),
    val earnedCashbackAmount: MonetaryAmount = 0.toMoney(BRL)
)