package br.com.beblue.core.application.order.query

import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.order.OrderId
import br.com.beblue.core.domain.player.PlayerId
import java.time.OffsetDateTime
import javax.money.MonetaryAmount

data class OrderQuery(
    val orderId: OrderId,
    val playerId: PlayerId,
    val occurredOn: OffsetDateTime,
    val totalAmount: MonetaryAmount,
    val totalPaidAmount: MonetaryAmount,
    val totalBurnedCashbackAmount: MonetaryAmount,
    val totalEarnedCashbackAmount: MonetaryAmount,
    val items: Set<Item>
) {
    data class Item(
        val card: Card,
        val cashbackPercentage: Int,
        val amount: MonetaryAmount,
        val paidAmount: MonetaryAmount,
        val burnedCashbackAmount: MonetaryAmount,
        val earnedCashbackAmount: MonetaryAmount
    ) {
        data class Card(
            val cardId: CardId,
            val name: String?,
            val number: Int?,
            val smallImageUri: String?,
            val largeImageUri: String?
        )
    }
}