package br.com.beblue.core.application.player

import br.com.beblue.core.application.player.command.PlayerRegistrationCommand
import br.com.beblue.core.application.player.query.PlayerQuery
import br.com.beblue.core.domain.card.CardRepository
import br.com.beblue.core.domain.player.Player
import br.com.beblue.core.domain.player.PlayerAlreadyExistsException
import br.com.beblue.core.domain.player.PlayerId
import br.com.beblue.core.domain.player.PlayerRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class PlayerService(
    private val playerRepository: PlayerRepository,
    private val cardRepository: CardRepository
) {

    @Transactional(readOnly = true)
    fun findPlayerByPlayerId(playerId: PlayerId) =
        playerRepository.findByPlayerId(playerId)?.toQuery()

    @Transactional
    fun registerPlayer(command: PlayerRegistrationCommand) {
        if (playerRepository.findByPlayerId(command.playerId) != null) {
            throw PlayerAlreadyExistsException(command.playerId)
        }

        val player = Player(
            playerId = command.playerId,
            name = command.name
        )

        playerRepository.create(player)
    }

    private fun Player.toQuery() = PlayerQuery(
        playerId = this.playerId,
        name = this.name,
        balance = PlayerQuery.Balance(
            availableAmount = this.balance.availableAmount,
            availableCashbackAmount = this.balance.availableCashbackAmount
        ),
        deck = this.deck.map { cardId ->
            cardRepository.findByCardId(cardId).let { card ->
                PlayerQuery.Card(
                    cardId = cardId,
                    name = card?.name,
                    number = card?.number,
                    smallImageUri = card?.smallImageUri.toString(),
                    largeImageUri = card?.largeImageUri.toString()
                )
            }
        }.toSet()
    )
}