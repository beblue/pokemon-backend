package br.com.beblue.core.application.player.command

import br.com.beblue.core.domain.player.PlayerId

data class PlayerRegistrationCommand(
    val playerId: PlayerId,
    val name: String
)