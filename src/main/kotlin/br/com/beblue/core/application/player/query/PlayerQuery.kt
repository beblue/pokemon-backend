package br.com.beblue.core.application.player.query

import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.player.PlayerId
import javax.money.MonetaryAmount

data class PlayerQuery(
    val playerId: PlayerId,
    val name: String,
    val balance: Balance,
    val deck: Set<Card>
) {
    data class Balance(
        val availableAmount: MonetaryAmount,
        val availableCashbackAmount: MonetaryAmount
    )

    data class Card(
        val cardId: CardId,
        val name: String?,
        val number: Int?,
        val smallImageUri: String?,
        val largeImageUri: String?
    )
}