package br.com.beblue.core.application.order.command

import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.player.PlayerId

data class OrderCreationCommand(
    val playerId: PlayerId,
    val items: Set<Item>
) {
    data class Item(
        val cardId: CardId
    )
}