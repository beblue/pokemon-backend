package br.com.beblue.core.application.card

import br.com.beblue.core.application.card.query.CardQuery
import br.com.beblue.core.domain.card.Card
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.card.CardRepository
import br.com.beblue.core.domain.card.Rarity
import br.com.beblue.core.domain.card.Type
import org.springframework.stereotype.Service

@Service
class CardService(private val cardRepository: CardRepository) {

    fun findCards(name: Set<String>, type: Set<Type>, rarity: Set<Rarity>) =
        cardRepository.find(name, type, rarity).map { it.toQuery() }

    fun findCardByCardId(cardId: CardId) =
        cardRepository.findByCardId(cardId)?.toQuery()

    private fun Card.toQuery() = CardQuery(
        cardId = this.cardId,
        name = this.name,
        number = this.number,
        hp = this.hp,
        rarity = this.rarity.toString(),
        type = this.type.toString(),
        subtype = this.subtype.toString(),
        evolvesFrom = this.evolvesFrom,
        ability = this.ability?.let { ability ->
            CardQuery.Ability(
                name = ability.name,
                text = ability.text
            )
        },
        attacks = this.attacks.map { attack ->
            CardQuery.Attack(
                name = attack.name,
                text = attack.text,
                damage = attack.damage,
                cost = attack.cost.map { cost -> cost.toString() }.toSet()
            )
        }.toSet(),
        resistances = this.resistances.map { entry ->
            CardQuery.Resistance(
                type = entry.key.toString(),
                value = entry.value
            )
        }.toSet(),
        weaknesses = this.weaknesses.map { entry ->
            CardQuery.Weakness(
                type = entry.key.toString(),
                value = entry.value
            )
        }.toSet(),
        amount = this.amount,
        cashbackPercentage = this.cashbackPercentage,
        smallImageUri = this.smallImageUri.toString(),
        largeImageUri = this.largeImageUri.toString()
    )
}