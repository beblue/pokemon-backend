package br.com.beblue.core.application.order

import br.com.beblue.core.application.order.command.OrderCreationCommand
import br.com.beblue.core.application.order.query.OrderQuery
import br.com.beblue.core.domain.card.CardRepository
import br.com.beblue.core.domain.order.Item
import br.com.beblue.core.domain.order.Order
import br.com.beblue.core.domain.order.OrderId
import br.com.beblue.core.domain.order.OrderRepository
import br.com.beblue.core.domain.player.PlayerId
import br.com.beblue.core.domain.player.PlayerRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.valiktor.functions.isNotEmpty
import org.valiktor.functions.isValid
import org.valiktor.functions.validateForEach
import org.valiktor.validate

@Service
class OrderService(
    private val orderRepository: OrderRepository,
    private val playerRepository: PlayerRepository,
    private val cardRepository: CardRepository
) {

    @Transactional(readOnly = true)
    fun findOrdersByPlayerId(playerId: PlayerId) =
        orderRepository.findByPlayerId(playerId).map { it.toQuery() }

    @Transactional(readOnly = true)
    fun findOrderByOrderId(orderId: OrderId) =
        orderRepository.findByOrderId(orderId)?.toQuery()

    @Transactional
    fun createNewOrder(command: OrderCreationCommand): OrderId {
        val player = playerRepository.findByPlayerId(command.playerId)

        val cards = command.items
            .mapNotNull { item -> cardRepository.findByCardId(item.cardId) }
            .map { it.cardId to it }
            .toMap()

        validate(command) {
            validate(OrderCreationCommand::playerId).isValid { player != null }
            validate(OrderCreationCommand::items).isNotEmpty().validateForEach {
                validate(OrderCreationCommand.Item::cardId).isValid { cards[it] != null }
            }
        }

        val order = Order(
            playerId = command.playerId,
            items = command.items
                .map { item -> cards.getValue(item.cardId) }
                .map { card -> Item(card.cardId, card.cashbackPercentage, card.amount) }
                .toSet()
        ).confirmPayment(
            availableAmount = player!!.balance.availableAmount,
            availableCashbackAmount = player.balance.availableCashbackAmount
        )

        orderRepository.create(order)

        playerRepository.update(
            player
                .addCards(order.items.map { it.cardId }.toSet())
                .updateBalance(
                    paidAmount = order.totalPaidAmount,
                    burnedCashbackAmount = order.totalBurnedCashbackAmount,
                    earnedCashbackAmount = order.totalEarnedCashbackAmount
                )
        )

        return order.orderId
    }

    private fun Order.toQuery() = OrderQuery(
        orderId = this.orderId,
        playerId = this.playerId,
        occurredOn = this.occurredOn,
        totalAmount = this.totalAmount,
        totalPaidAmount = this.totalPaidAmount,
        totalBurnedCashbackAmount = this.totalBurnedCashbackAmount,
        totalEarnedCashbackAmount = this.totalEarnedCashbackAmount,
        items = this.items.map { item ->
            OrderQuery.Item(
                card = cardRepository.findByCardId(item.cardId).let { card ->
                    OrderQuery.Item.Card(
                        cardId = item.cardId,
                        name = card?.name,
                        number = card?.number,
                        smallImageUri = card?.smallImageUri.toString(),
                        largeImageUri = card?.largeImageUri.toString()
                    )
                },
                cashbackPercentage = item.cashbackPercentage,
                amount = item.amount,
                paidAmount = item.paidAmount,
                burnedCashbackAmount = item.burnedCashbackAmount,
                earnedCashbackAmount = item.earnedCashbackAmount
            )
        }.toSet()
    )
}