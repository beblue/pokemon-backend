package br.com.beblue.core.application.card.query

import br.com.beblue.core.domain.card.CardId
import javax.money.MonetaryAmount

data class CardQuery(
    val cardId: CardId,
    val name: String,
    val number: Int,
    val hp: Int,
    val rarity: String,
    val type: String,
    val subtype: String,
    val evolvesFrom: String?,
    val ability: Ability?,
    val attacks: Set<Attack>,
    val resistances: Set<Resistance>,
    val weaknesses: Set<Weakness>,
    val amount: MonetaryAmount,
    val cashbackPercentage: Int,
    val smallImageUri: String,
    val largeImageUri: String
) {
    data class Ability(
        val name: String,
        val text: String
    )

    data class Attack(
        val name: String,
        val text: String,
        val damage: String,
        val cost: Set<String>
    )

    data class Resistance(
        val type: String,
        val value: String
    )

    data class Weakness(
        val type: String,
        val value: String
    )
}