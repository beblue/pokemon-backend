package br.com.beblue.adapters.jpa.repository

import br.com.beblue.adapters.jpa.entity.OrderEntity
import br.com.beblue.adapters.jpa.entity.toEntity
import br.com.beblue.core.domain.order.Order
import br.com.beblue.core.domain.order.OrderId
import br.com.beblue.core.domain.order.OrderRepository
import br.com.beblue.core.domain.player.PlayerId
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager

@Repository
class OrderRepositoryJpaAdapter(private val entityManager: EntityManager) : OrderRepository {

    override fun findByPlayerId(playerId: PlayerId): Collection<Order> =
        entityManager.createQuery("select o from OrderEntity o where o.playerId = :playerId", OrderEntity::class.java)
            .setParameter("playerId", playerId)
            .resultList
            .map { it.toOrder() }

    override fun findByOrderId(orderId: OrderId): Order? =
        entityManager.find(OrderEntity::class.java, orderId)?.toOrder()

    override fun create(order: Order) =
        entityManager.persist(order.toEntity())
}