package br.com.beblue.adapters.jpa.entity

import br.com.beblue.core.domain.player.Balance
import br.com.beblue.core.domain.player.Player
import org.hibernate.annotations.Columns
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency
import javax.money.MonetaryAmount
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "player")
@TypeDef(name = "moneyType", typeClass = PersistentMoneyAmountAndCurrency::class)
data class PlayerEntity(

    @Id
    @Column(name = "player_id")
    val playerId: String,

    @Column(name = "name")
    val name: String,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "available_amount_currency"), Column(name = "available_amount")])
    val availableAmount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "available_cashback_amount_currency"), Column(name = "available_cashback_amount")])
    val availableCashbackAmount: MonetaryAmount,

    @OneToMany(mappedBy = "player", cascade = [CascadeType.ALL], orphanRemoval = true)
    val deck: Set<DeckEntity>
) {
    fun toPlayer() = Player(
        playerId = this.playerId,
        name = this.name,
        balance = Balance(
            availableAmount = this.availableAmount,
            availableCashbackAmount = this.availableCashbackAmount
        ),
        deck = this.deck.map { it.cardId }.toSet()
    )
}

fun Player.toEntity() = PlayerEntity(
    playerId = this.playerId,
    name = this.name,
    availableAmount = this.balance.availableAmount,
    availableCashbackAmount = this.balance.availableCashbackAmount,
    deck = this.deck.map { cardId -> DeckEntity(cardId = cardId) }.toSet()
).apply { this.deck.forEach { it.player = this } }