package br.com.beblue.adapters.jpa.entity

import br.com.beblue.core.domain.order.Order
import br.com.beblue.core.domain.order.OrderStatus
import org.hibernate.annotations.Columns
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency
import java.time.OffsetDateTime
import java.util.UUID
import javax.money.MonetaryAmount
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "orders")
@TypeDef(name = "moneyType", typeClass = PersistentMoneyAmountAndCurrency::class)
data class OrderEntity(

    @Id
    @Column(name = "order_id")
    val orderId: UUID,

    @Column(name = "player_id")
    val playerId: String,

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    val status: OrderStatus,

    @Column(name = "occurred_on")
    val occurredOn: OffsetDateTime = OffsetDateTime.now(),

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "total_amount_currency"), Column(name = "total_amount")])
    val totalAmount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "total_paid_amount_currency"), Column(name = "total_paid_amount")])
    val totalPaidAmount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "total_burned_cashback_amount_currency"), Column(name = "total_burned_cashback_amount")])
    val totalBurnedCashbackAmount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "total_earned_cashback_amount_currency"), Column(name = "total_earned_cashback_amount")])
    val totalEarnedCashbackAmount: MonetaryAmount,

    @OneToMany(mappedBy = "order", cascade = [CascadeType.ALL], orphanRemoval = true)
    val items: Set<ItemEntity>
) {
    fun toOrder() = Order(
        orderId = this.orderId,
        playerId = this.playerId,
        status = this.status,
        items = this.items.map { it.toItem() }.toSet()
    )
}

fun Order.toEntity() = OrderEntity(
    orderId = this.orderId,
    playerId = this.playerId,
    occurredOn = this.occurredOn,
    status = this.status,
    totalAmount = this.totalAmount,
    totalPaidAmount = this.totalPaidAmount,
    totalBurnedCashbackAmount = this.totalBurnedCashbackAmount,
    totalEarnedCashbackAmount = this.totalEarnedCashbackAmount,
    items = this.items.map { it.toEntity() }.toSet()
).apply { this.items.forEach { it.order = this } }