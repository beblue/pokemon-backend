package br.com.beblue.adapters.jpa.entity

import br.com.beblue.core.domain.order.Item
import org.hibernate.annotations.Columns
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency
import java.io.Serializable
import java.util.UUID
import javax.money.MonetaryAmount
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

private data class ItemEntityPK(
    val order: UUID = UUID.randomUUID(),
    val cardId: String = ""
) : Serializable

@Entity
@Table(name = "item")
@IdClass(ItemEntityPK::class)
@TypeDef(name = "moneyType", typeClass = PersistentMoneyAmountAndCurrency::class)
data class ItemEntity(

    @Id
    @Column(name = "card_id")
    val cardId: String,

    @Column(name = "cashback_percentage")
    val cashbackPercentage: Int,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "amount_currency"), Column(name = "amount")])
    val amount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "paid_amount_currency"), Column(name = "paid_amount")])
    val paidAmount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "burned_cashback_amount_currency"), Column(name = "burned_cashback_amount")])
    val burnedCashbackAmount: MonetaryAmount,

    @Type(type = "moneyType")
    @Columns(columns = [Column(name = "earned_cashback_amount_currency"), Column(name = "earned_cashback_amount")])
    val earnedCashbackAmount: MonetaryAmount
) {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    var order: OrderEntity? = null

    fun toItem() = Item(
        cardId = this.cardId,
        cashbackPercentage = this.cashbackPercentage,
        amount = this.amount,
        paidAmount = this.paidAmount,
        burnedCashbackAmount = this.burnedCashbackAmount,
        earnedCashbackAmount = this.earnedCashbackAmount
    )
}

fun Item.toEntity() = ItemEntity(
    cardId = this.cardId,
    cashbackPercentage = this.cashbackPercentage,
    amount = this.amount,
    paidAmount = this.paidAmount,
    burnedCashbackAmount = this.burnedCashbackAmount,
    earnedCashbackAmount = this.earnedCashbackAmount
)