package br.com.beblue.adapters.jpa.repository

import br.com.beblue.adapters.jpa.entity.PlayerEntity
import br.com.beblue.adapters.jpa.entity.toEntity
import br.com.beblue.core.domain.player.Player
import br.com.beblue.core.domain.player.PlayerId
import br.com.beblue.core.domain.player.PlayerRepository
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager

@Repository
class PlayerRepositoryJpaAdapter(private val entityManager: EntityManager) : PlayerRepository {

    override fun findByPlayerId(playerId: PlayerId): Player? =
        entityManager.find(PlayerEntity::class.java, playerId)?.toPlayer()

    override fun create(player: Player) =
        entityManager.persist(player.toEntity())

    override fun update(player: Player) =
        entityManager.merge(player.toEntity()).let {}
}