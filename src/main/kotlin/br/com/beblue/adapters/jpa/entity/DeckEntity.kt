package br.com.beblue.adapters.jpa.entity

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

private data class DeckEntityPK(
    val player: String = "",
    val cardId: String = ""
) : Serializable

@Entity
@Table(name = "deck")
@IdClass(DeckEntityPK::class)
data class DeckEntity(
    @Id
    @Column(name = "card_id")
    val cardId: String
) {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_id")
    var player: PlayerEntity? = null
}