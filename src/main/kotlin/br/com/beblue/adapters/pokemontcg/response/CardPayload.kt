package br.com.beblue.adapters.pokemontcg.response

import br.com.beblue.core.domain.card.Card
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.card.Rarity
import br.com.beblue.core.domain.card.Subtype
import br.com.beblue.core.domain.card.Type
import java.net.URI

data class CardPayload(
    val id: CardId,
    val name: String,
    val number: String,
    val hp: String,
    val rarity: String,
    val types: Set<String>?,
    val subtype: String,
    val evolvesFrom: String?,
    val ability: Ability?,
    val attacks: Set<Attack>?,
    val resistances: Set<Resistance>?,
    val weaknesses: Set<Weakness>?,
    val imageUrl: String,
    val imageUrlHiRes: String
) {
    data class Ability(
        val name: String,
        val text: String
    )

    data class Attack(
        val name: String,
        val text: String,
        val damage: String,
        val cost: Set<String>?
    )

    data class Resistance(
        val type: String,
        val value: String
    )

    data class Weakness(
        val type: String,
        val value: String
    )

    fun toCard() = Card(
        cardId = this.id,
        name = this.name,
        number = this.number.toInt(),
        hp = this.hp.toInt(),
        rarity = this.rarity.toRarity(),
        type = this.types.orEmpty().first().toType(),
        subtype = this.subtype.toSubType(),
        evolvesFrom = this.evolvesFrom,
        ability = this.ability?.let { ability ->
            br.com.beblue.core.domain.card.Ability(
                name = ability.name,
                text = ability.text
            )
        },
        attacks = this.attacks.orEmpty().map { attack ->
            br.com.beblue.core.domain.card.Attack(
                name = attack.name,
                text = attack.text,
                damage = attack.damage,
                cost = attack.cost.orEmpty().map { it.toType() }.toSet()
            )
        }.toSet(),
        resistances = this.resistances.orEmpty().map { it.type.toType() to it.value }.toMap(),
        weaknesses = this.weaknesses.orEmpty().map { it.type.toType() to it.value }.toMap(),
        smallImageUri = URI.create(this.imageUrl),
        largeImageUri = URI.create(this.imageUrlHiRes)
    )

    private fun String.toRarity() = Rarity.fromString(this)
        ?: throw EnumConstantNotPresentException(Rarity::class.java, this)

    private fun String.toType() = Type.fromString(this)
        ?: throw EnumConstantNotPresentException(Type::class.java, this)

    private fun String.toSubType() = Subtype.fromString(this)
        ?: throw EnumConstantNotPresentException(Subtype::class.java, this)
}