package br.com.beblue.adapters.pokemontcg.repository

import br.com.beblue.adapters.pokemontcg.response.CardResponse
import br.com.beblue.adapters.pokemontcg.response.CardsResponse
import br.com.beblue.core.domain.card.Card
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.card.CardRepository
import br.com.beblue.core.domain.card.Rarity
import br.com.beblue.core.domain.card.Type
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

private const val CACHE_CONTROL = "no-cache"
private const val USER_AGENT = "Beblue/Pokemon"

private const val SETCODE = "base1"
private const val SUPERTYPE = "Pokemon"
private const val SEPARATOR = "|"

@Component
class CardRepositoryRestAdapter(private val restTemplate: RestTemplate) : CardRepository {

    private val defaultEntity = HttpEntity<Any>(
        HttpHeaders().apply {
            this[HttpHeaders.ACCEPT] = MediaType.APPLICATION_JSON_VALUE
            this[HttpHeaders.CACHE_CONTROL] = CACHE_CONTROL
            this[HttpHeaders.USER_AGENT] = USER_AGENT
        }
    )

    @Cacheable("cards")
    override fun find(name: Set<String>, type: Set<Type>, rarity: Set<Rarity>) =
        restTemplate.exchange<CardsResponse>(
            url = "/cards?setCode={setCode}&supertype={supertype}&name={name}&types={types}&rarity={rarity}",
            method = HttpMethod.GET,
            requestEntity = defaultEntity,
            uriVariables = mapOf(
                "setCode" to SETCODE,
                "supertype" to SUPERTYPE,
                "name" to name.joinToString(separator = SEPARATOR),
                "types" to type.joinToString(separator = SEPARATOR),
                "rarity" to rarity.joinToString(separator = SEPARATOR)
            )
        ).body?.cards.orEmpty().map { it.toCard() }.sortedBy { it.number }

    @Cacheable("card")
    override fun findByCardId(cardId: CardId): Card? =
        restTemplate.exchange<CardResponse>(
            url = "/cards/{cardId}",
            method = HttpMethod.GET,
            requestEntity = defaultEntity,
            uriVariables = mapOf(
                "cardId" to cardId
            )
        ).body?.card?.toCard()
}