package br.com.beblue.adapters.pokemontcg

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class RestTemplateConfig(@Value("\${pokemontcg.root-uri}") private val rootUri: String) {

    @Bean
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate = builder.rootUri(rootUri).build()
}