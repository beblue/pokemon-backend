package br.com.beblue.adapters.pokemontcg.response

data class CardsResponse(
    val cards: Set<CardPayload>
)