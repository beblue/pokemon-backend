package br.com.beblue.adapters.pokemontcg.response

data class CardResponse(
    val card: CardPayload
)