package br.com.beblue.adapters.rest.controller

import br.com.beblue.core.application.card.CardService
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.card.Rarity
import br.com.beblue.core.domain.card.Type
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/cards")
class CardController(private val cardService: CardService) {

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findCards(
        @RequestParam(name = "name", required = false, defaultValue = "") name: Set<String>,
        @RequestParam(name = "type", required = false, defaultValue = "") type: Set<String>,
        @RequestParam(name = "rarity", required = false, defaultValue = "") rarity: Set<String>
    ) = ResponseEntity.ok(
        cardService.findCards(
            name = name,
            type = type.mapNotNull { Type.fromString(it) }.toSet(),
            rarity = rarity.mapNotNull { Rarity.fromString(it) }.toSet()
        )
    )

    @GetMapping("/{cardId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findCardByCardId(@PathVariable("cardId") cardId: CardId) =
        cardService.findCardByCardId(cardId)
            ?.let { ResponseEntity.ok(it) }
            ?: ResponseEntity.notFound().build<Void>()
}