package br.com.beblue.adapters.rest.controller

import br.com.beblue.core.application.order.OrderService
import br.com.beblue.core.application.player.PlayerService
import br.com.beblue.core.application.player.command.PlayerRegistrationCommand
import br.com.beblue.core.domain.player.PlayerAlreadyExistsException
import br.com.beblue.core.domain.player.PlayerId
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri

@RestController
@RequestMapping("/v1/players")
class PlayerController(
    private val playerService: PlayerService,
    private val orderService: OrderService
) {

    @GetMapping("/{playerId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findPlayerByPlayerId(@PathVariable("playerId") playerId: PlayerId) =
        playerService.findPlayerByPlayerId(playerId)
            ?.let { ResponseEntity.ok(it) }
            ?: ResponseEntity.notFound().build<Void>()

    @GetMapping("/{playerId}/orders", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findOrdersByPlayerId(@PathVariable("playerId") playerId: PlayerId) =
        playerService.findPlayerByPlayerId(playerId)
            ?.let { orderService.findOrdersByPlayerId(playerId) }
            ?.let { ResponseEntity.ok(it) }
            ?: ResponseEntity.notFound().build<Void>()

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun registerPlayer(@RequestBody command: PlayerRegistrationCommand) =
        playerService.registerPlayer(command)
            .let { fromCurrentRequestUri().path("/{playerId}").buildAndExpand(command.playerId).toUri() }
            .let { ResponseEntity.created(it).build<Void>() }

    @ExceptionHandler(PlayerAlreadyExistsException::class)
    fun handlePlayerAlreadyExistsException(ex: PlayerAlreadyExistsException) =
        ResponseEntity
            .status(HttpStatus.CONFLICT)
            .body(playerService.findPlayerByPlayerId(ex.playerId))
}