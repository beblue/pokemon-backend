package br.com.beblue.adapters.rest

import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.zalando.jackson.datatype.money.MoneyModule

@Configuration
class JacksonConfig {

    @Bean
    fun kotlinModule() = KotlinModule()

    @Bean
    fun moneyModule() = MoneyModule()
}