package br.com.beblue.adapters.rest.controller

import br.com.beblue.core.application.order.OrderService
import br.com.beblue.core.application.order.command.OrderCreationCommand
import br.com.beblue.core.domain.order.InsufficientBalanceException
import br.com.beblue.core.domain.order.OrderId
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri

@RestController
@RequestMapping("/v1/orders")
class OrderController(private val orderService: OrderService) {

    @GetMapping("/{orderId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findOrderByOrderId(@PathVariable("orderId") orderId: OrderId) =
        orderService.findOrderByOrderId(orderId)
            ?.let { ResponseEntity.ok(it) }
            ?: ResponseEntity.notFound().build<Void>()

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun registerOrder(@RequestBody command: OrderCreationCommand) =
        orderService.createNewOrder(command)
            .let { fromCurrentRequestUri().path("/{orderId}").buildAndExpand(it).toUri() }
            .let { ResponseEntity.created(it).build<Void>() }

    @ExceptionHandler(InsufficientBalanceException::class)
    fun handlePlayerAlreadyExistsException(ex: InsufficientBalanceException) =
        ResponseEntity
            .status(HttpStatus.FORBIDDEN)
            .build<Void>()
}