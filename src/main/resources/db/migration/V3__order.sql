create table order_status (order_status_id varchar(20) not null primary key);
insert into  order_status (order_status_id) values ('PENDING');
insert into  order_status (order_status_id) values ('CONFIRMED');

create table orders (
  order_id                              uuid not null primary key,
  player_id                             varchar(11) not null references player(player_id),
  status                                varchar(20) not null references order_status(order_status_id),
  occurred_on                           timestamp with time zone not null,
  total_amount                          decimal(20, 2) not null,
  total_amount_currency                 varchar(3) not null references currency(currency_id),
  total_paid_amount                     decimal(20, 2) not null,
  total_paid_amount_currency            varchar(3) not null references currency(currency_id),
  total_burned_cashback_amount          decimal(20, 2) not null,
  total_burned_cashback_amount_currency varchar(3) not null references currency(currency_id),
  total_earned_cashback_amount          decimal(20, 2) not null,
  total_earned_cashback_amount_currency varchar(3) not null references currency(currency_id)
);

create table item (
  order_id                        uuid not null references orders(order_id),
  card_id                         varchar(20) not null,
  cashback_percentage             int not null,
  amount                          decimal(20, 2) not null,
  amount_currency                 varchar(3) not null references currency(currency_id),
  paid_amount                     decimal(20, 2) not null,
  paid_amount_currency            varchar(3) not null references currency(currency_id),
  burned_cashback_amount          decimal(20, 2) not null,
  burned_cashback_amount_currency varchar(3) not null references currency(currency_id),
  earned_cashback_amount          decimal(20, 2) not null,
  earned_cashback_amount_currency varchar(3) not null references currency(currency_id),

  primary key (order_id, card_id)
);