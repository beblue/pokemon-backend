create table player (
  player_id                          varchar(11) not null primary key,
  name                               varchar(50) not null,
  available_amount                   decimal(20, 2) not null,
  available_amount_currency          varchar(3) not null references currency(currency_id),
  available_cashback_amount          decimal(20, 2) not null,
  available_cashback_amount_currency varchar(3) not null references currency(currency_id)
);

create table deck (
  player_id varchar(11) not null,
  card_id   varchar(20) not null,

  primary key(player_id, card_id)
);