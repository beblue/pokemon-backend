package br.com.beblue.core.domain.card

import java.net.URI

object CardTestFixture {

    val charizard = Card(
        cardId = "base1-4",
        name = "Charizard",
        number = 4,
        hp = 120,
        rarity = Rarity.RARE,
        type = Type.FIRE,
        subtype = Subtype.STAGE_2,
        evolvesFrom = "Charmeleon",
        ability = Ability(
            name = "Energy Burn",
            text = "As often as you like during your turn (before your attack), you may turn all Energy attached to Charizard into R for the rest of the turn. This power can't be used if Charizard is Asleep, Confused, or Paralyzed."
        ),
        attacks = setOf(
            Attack(
                name = "Fire Spin",
                text = "Discard 2 Energy cards attached to Charizard in order to use this attack.",
                damage = "100",
                cost = setOf(Type.FIRE)
            )
        ),
        resistances = mapOf(
            Type.FIGHTING to "-30"
        ),
        weaknesses = mapOf(
            Type.WATER to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/4.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/4_hires.png")
    )

    val dewgong = Card(
        cardId = "base1-25",
        name = "Dewgong",
        number = 25,
        hp = 80,
        rarity = Rarity.UNCOMMON,
        type = Type.WATER,
        subtype = Subtype.STAGE_1,
        evolvesFrom = "Seel",
        ability = null,
        attacks = setOf(
            Attack(
                name = "Ice Beam",
                text = "Flip a coin. If heads, the Defending Pokémon is now Paralyzed.",
                damage = "30",
                cost = setOf(
                    Type.WATER,
                    Type.COLORLESS
                )
            ),
            Attack(
                name = "Aurora Beam",
                text = "",
                damage = "50",
                cost = setOf(
                    Type.WATER,
                    Type.COLORLESS
                )
            )
        ),
        resistances = emptyMap(),
        weaknesses = mapOf(
            Type.LIGHTNING to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/25.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/25_hires.png")
    )

    val bulbasaur = Card(
        cardId = "base1-44",
        name = "Bulbasaur",
        number = 44,
        hp = 40,
        rarity = Rarity.COMMON,
        type = Type.GRASS,
        subtype = Subtype.BASIC,
        evolvesFrom = null,
        ability = null,
        attacks = setOf(
            Attack(
                name = "Leech Seed",
                text = "Unless all damage from this attack is prevented, you may remove 1 damage counter from Bulbasaur.",
                damage = "20",
                cost = setOf(Type.GRASS)
            )
        ),
        resistances = emptyMap(),
        weaknesses = mapOf(
            Type.FIRE to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/44.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/44_hires.png")
    )

    val alakazam = Card(
        cardId = "base1-1",
        name = "Alakazam",
        number = 1,
        hp = 80,
        rarity = Rarity.RARE,
        type = Type.PSYCHIC,
        subtype = Subtype.STAGE_2,
        evolvesFrom = "Kadabra",
        ability = Ability(
            name = "Damage Swap",
            text = "As often as you like during your turn (before your attack), you may move 1 damage counter from 1 of your Pokémon to another as long as you don't Knock Out that Pokémon. This power can't be used if Alakazam is Asleep, Confused, or Paralyzed."
        ),
        attacks = setOf(
            Attack(
                name = "Confuse Ray",
                text = "Flip a coin. If heads, defender is now Confused.",
                damage = "30",
                cost = setOf(Type.PSYCHIC)
            )
        ),
        resistances = emptyMap(),
        weaknesses = mapOf(
            Type.PSYCHIC to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/1.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/1_hires.png")
    )

    val chansey = Card(
        cardId = "base1-3",
        name = "Chansey",
        number = 3,
        hp = 120,
        rarity = Rarity.RARE,
        type = Type.COLORLESS,
        subtype = Subtype.BASIC,
        evolvesFrom = null,
        ability = null,
        attacks = setOf(
            Attack(
                name = "Scrunch",
                text = "Flip a coin. If heads, prevent all damage done to Chansey during your opponent's next turn. (Any other effects of attacks still happen.)",
                damage = "",
                cost = setOf(Type.COLORLESS)
            ),
            Attack(
                name = "Double-edge",
                text = "Chansey does 80 damage to itself.",
                damage = "80",
                cost = setOf(Type.COLORLESS)
            )
        ),
        resistances = mapOf(
            Type.PSYCHIC to "-30"
        ),
        weaknesses = mapOf(
            Type.FIGHTING to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/3.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/3_hires.png")
    )

    val machoke = Card(
        cardId = "base1-34",
        name = "Machoke",
        number = 34,
        hp = 80,
        rarity = Rarity.UNCOMMON,
        type = Type.FIGHTING,
        subtype = Subtype.STAGE_1,
        evolvesFrom = "Machop",
        ability = null,
        attacks = setOf(
            Attack(
                name = "Karate Chop",
                text = "Does 50 damage minus 10 for each damage counter on Machoke.",
                damage = "50-",
                cost = setOf(
                    Type.FIGHTING,
                    Type.COLORLESS
                )
            ),
            Attack(
                name = "Submission",
                text = "Machoke does 20 damage to itself.",
                damage = "60",
                cost = setOf(
                    Type.FIGHTING,
                    Type.COLORLESS
                )
            )
        ),
        resistances = emptyMap(),
        weaknesses = mapOf(
            Type.PSYCHIC to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/34.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/34_hires.png")
    )

    val voltorb = Card(
        cardId = "base1-67",
        name = "Voltorb",
        number = 67,
        hp = 40,
        rarity = Rarity.COMMON,
        type = Type.LIGHTNING,
        subtype = Subtype.BASIC,
        evolvesFrom = null,
        ability = null,
        attacks = setOf(
            Attack(
                name = "Tackle",
                text = "",
                damage = "10",
                cost = setOf(Type.COLORLESS)
            )
        ),
        resistances = emptyMap(),
        weaknesses = mapOf(
            Type.FIGHTING to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/67.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/67_hires.png")
    )
}