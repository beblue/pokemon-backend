package br.com.beblue.core.domain.order

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.toMoney
import javax.money.CurrencyUnit
import javax.money.Monetary

object OrderTestFixture {

    val invalidCurrency: CurrencyUnit = Monetary.getCurrency("USD")

    val pendingOrder = Order(
        playerId = "50732984033",
        items = setOf(
            Item(
                cardId = "base1-1",
                cashbackPercentage = 30,
                amount = 100.toMoney(BRL)
            ),
            Item(
                cardId = "base1-10",
                cashbackPercentage = 10,
                amount = 74.99.toMoney(BRL)
            ),
            Item(
                cardId = "base1-20",
                cashbackPercentage = 5,
                amount = 47.99.toMoney(BRL)
            )
        )
    )

    val confirmedOrder = Order(
        orderId = pendingOrder.orderId,
        status = OrderStatus.CONFIRMED,
        playerId = "50732984033",
        items = setOf(
            Item(
                cardId = "base1-1",
                cashbackPercentage = 30,
                amount = 100.toMoney(BRL),
                paidAmount = 100.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 30.toMoney(BRL)
            ),
            Item(
                cardId = "base1-10",
                cashbackPercentage = 10,
                amount = 74.99.toMoney(BRL),
                paidAmount = 74.99.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 7.50.toMoney(BRL)
            ),
            Item(
                cardId = "base1-20",
                cashbackPercentage = 5,
                amount = 47.99.toMoney(BRL),
                paidAmount = 47.01.toMoney(BRL),
                burnedCashbackAmount = 0.98.toMoney(BRL),
                earnedCashbackAmount = 2.35.toMoney(BRL)
            )
        )
    )
}