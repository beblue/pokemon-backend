package br.com.beblue.core.domain.player

import br.com.beblue.containViolation
import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.CPF
import br.com.beblue.core.domain.toMoney
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.DescribeSpec
import org.valiktor.ConstraintViolationException
import org.valiktor.constraints.NotBlank
import org.valiktor.constraints.Size

class PlayerTest : DescribeSpec({

    describe("Validate player") {

        context("Invalid playerId") {

            it("Should fail when playerId is empty") {
                val ex = shouldThrow<ConstraintViolationException> {
                    PlayerTestFixture.validPlayer.copy(playerId = "")
                }
                ex should containViolation("playerId", "", NotBlank)
            }

            it("Should fail when playerId is blank") {
                val ex = shouldThrow<ConstraintViolationException> {
                    PlayerTestFixture.validPlayer.copy(playerId = "   ")
                }
                ex should containViolation("playerId", "   ", NotBlank)
            }

            it("Should fail when playerId is invalid CPF") {
                val ex = shouldThrow<ConstraintViolationException> {
                    PlayerTestFixture.validPlayer.copy(playerId = "11111111111")
                }
                ex should containViolation("playerId", "11111111111", CPF)
            }
        }

        context("Invalid name") {

            it("Should fail when name is empty") {
                val ex = shouldThrow<ConstraintViolationException> {
                    PlayerTestFixture.validPlayer.copy(name = "")
                }
                ex should containViolation("name", "", NotBlank)
            }

            it("Should fail when name is blank") {
                val ex = shouldThrow<ConstraintViolationException> {
                    PlayerTestFixture.validPlayer.copy(name = "   ")
                }
                ex should containViolation("name", "   ", NotBlank)
            }

            it("Should fail when name size is greater than 50") {
                val ex = shouldThrow<ConstraintViolationException> {
                    PlayerTestFixture.validPlayer.copy(name = (1..51).joinToString(separator = "") { "!" })
                }
                ex should containViolation("name", (1..51).joinToString(separator = "") { "!" }, Size(max = 50))
            }
        }
    }

    describe("Create new player") {

        it("Should create new player with default balance") {

            PlayerTestFixture.validPlayer.balance shouldBe Balance(
                availableAmount = 100.toMoney(BRL),
                availableCashbackAmount = 0.toMoney(BRL)
            )
        }
    }

    describe("Update balance") {

        it("Should update balance") {

            val player = PlayerTestFixture.validPlayer.updateBalance(
                paidAmount = 70.toMoney(BRL),
                burnedCashbackAmount = 5.toMoney(BRL),
                earnedCashbackAmount = 10.toMoney(BRL)
            )

            player.balance shouldBe Balance(
                availableAmount = 30.toMoney(BRL),
                availableCashbackAmount = 5.toMoney(BRL)
            )
        }
    }

    describe("Add cards to deck") {

        it("Should add cards to deck") {

            val player = PlayerTestFixture.validPlayer.addCards(setOf("base1-1", "base1-20", "base1-20"))
            player.deck shouldBe setOf("base1-1", "base1-20", "base1-20")
        }
    }
})