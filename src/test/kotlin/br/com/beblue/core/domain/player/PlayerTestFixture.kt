package br.com.beblue.core.domain.player

import javax.money.CurrencyUnit
import javax.money.Monetary

object PlayerTestFixture {

    val invalidCurrency: CurrencyUnit = Monetary.getCurrency("USD")

    val validPlayer = Player(
        playerId = "50732984033",
        name = "Player 1"
    )
}