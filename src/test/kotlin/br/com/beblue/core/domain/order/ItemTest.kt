package br.com.beblue.core.domain.order

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.toMoney
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec

class ItemTest : DescribeSpec({

    describe("Create item") {

        it("Should create a new item") {
            ItemTestFixture.validItem shouldBe Item(
                cardId = "base1-1",
                cashbackPercentage = 30,
                amount = 100.toMoney(BRL),
                paidAmount = 0.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 0.toMoney(BRL)
            )
        }
    }
})