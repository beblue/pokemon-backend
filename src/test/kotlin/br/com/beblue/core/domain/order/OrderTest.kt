package br.com.beblue.core.domain.order

import br.com.beblue.containViolation
import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.toMoney
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.DescribeSpec
import org.valiktor.ConstraintViolationException
import org.valiktor.constraints.NotEmpty

class OrderTest : DescribeSpec({

    describe("Validate order") {

        context("Invalid items") {

            it("Should fail when items is empty") {
                val ex = shouldThrow<ConstraintViolationException> {
                    OrderTestFixture.pendingOrder.copy(items = emptySet())
                }
                ex should containViolation("items", emptySet<Item>(), NotEmpty)
            }
        }
    }

    describe("Create new order") {

        it("Should create pending order and calculate total amounts") {

            OrderTestFixture.pendingOrder.status shouldBe OrderStatus.PENDING
            OrderTestFixture.pendingOrder.totalAmount shouldBe 222.98.toMoney(BRL)
            OrderTestFixture.pendingOrder.totalPaidAmount shouldBe 0.toMoney(BRL)
            OrderTestFixture.pendingOrder.totalBurnedCashbackAmount shouldBe 0.toMoney(BRL)
            OrderTestFixture.pendingOrder.totalEarnedCashbackAmount shouldBe 0.toMoney(BRL)
        }
    }

    describe("Confirm payment") {

        context("Insufficient balance") {

            it("Should fail when total available amount is insufficient") {

                val ex = shouldThrow<InsufficientBalanceException> {
                    OrderTestFixture.pendingOrder.confirmPayment(
                        availableAmount = 222.toMoney(BRL),
                        availableCashbackAmount = 0.97.toMoney(BRL)
                    )
                }

                ex.amount shouldBe 222.98.toMoney(BRL)
                ex.availableAmount shouldBe 222.97.toMoney(BRL)
            }
        }

        it("Should confirm payment and calculate total amounts") {

            val order = OrderTestFixture.pendingOrder.confirmPayment(
                availableAmount = 222.toMoney(BRL),
                availableCashbackAmount = 0.98.toMoney(BRL)
            )
            order shouldBe OrderTestFixture.confirmedOrder

            order.totalAmount shouldBe 222.98.toMoney(BRL)
            order.totalPaidAmount shouldBe 222.toMoney(BRL)
            order.totalBurnedCashbackAmount shouldBe 0.98.toMoney(BRL)
            order.totalEarnedCashbackAmount shouldBe 39.85.toMoney(BRL)
        }
    }
})