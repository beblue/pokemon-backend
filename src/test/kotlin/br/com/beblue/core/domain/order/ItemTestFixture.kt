package br.com.beblue.core.domain.order

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.toMoney

object ItemTestFixture {

    val validItem = Item(
        cardId = "base1-1",
        cashbackPercentage = 30,
        amount = 100.toMoney(BRL)
    )
}