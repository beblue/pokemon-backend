package br.com.beblue.core.domain.card

import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.toMoney
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec

class CardTest : DescribeSpec({

    describe("Calculate cashbackPercentage") {

        it("cashbackPercentage from FIRE type should be 5%") {
            CardTestFixture.charizard.cashbackPercentage shouldBe 5
        }

        it("cashbackPercentage from WATER type should be 10%") {
            CardTestFixture.dewgong.cashbackPercentage shouldBe 10
        }

        it("cashbackPercentage from GRASS type should be 15%") {
            CardTestFixture.bulbasaur.cashbackPercentage shouldBe 15
        }

        it("cashbackPercentage from PSYCHIC type should be 20%") {
            CardTestFixture.alakazam.cashbackPercentage shouldBe 20
        }

        it("cashbackPercentage from COLORLESS type should be 25%") {
            CardTestFixture.chansey.cashbackPercentage shouldBe 25
        }

        it("cashbackPercentage from FIGHTING type should be 30%") {
            CardTestFixture.machoke.cashbackPercentage shouldBe 30
        }

        it("cashbackPercentage from LIGHTNING type should be 35%") {
            CardTestFixture.voltorb.cashbackPercentage shouldBe 35
        }
    }

    describe("Calculate amount") {

        it("amount from Charizard should be R$ 15,60") {
            CardTestFixture.charizard.amount shouldBe 15.6.toMoney(BRL)
        }

        it("amount from Dewgong should be R$ 6,40") {
            CardTestFixture.dewgong.amount shouldBe 6.4.toMoney(BRL)
        }

        it("amount from Bulbasaur should be R$ 1,20") {
            CardTestFixture.bulbasaur.amount shouldBe 1.2.toMoney(BRL)
        }

        it("amount from Alakazam should be R$ 10,40") {
            CardTestFixture.alakazam.amount shouldBe 10.4.toMoney(BRL)
        }

        it("amount from Chansey should be R$ 10,80") {
            CardTestFixture.chansey.amount shouldBe 10.8.toMoney(BRL)
        }

        it("amount from Machoke should be R$ 6,40") {
            CardTestFixture.machoke.amount shouldBe 6.4.toMoney(BRL)
        }

        it("amount from Voltorb should be R$ 1,20") {
            CardTestFixture.voltorb.amount shouldBe 1.2.toMoney(BRL)
        }
    }
})