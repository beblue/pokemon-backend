package br.com.beblue.core.application.card

import br.com.beblue.core.domain.card.CardRepository
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import io.mockk.clearMocks
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify

class CardServiceTest : DescribeSpec() {

    private val cardRepository = mockk<CardRepository>()
    private val cardService = CardService(cardRepository)

    override fun afterTest(testCase: TestCase, result: TestResult) = clearMocks(cardRepository)

    init {
        describe("Find cards") {

            context("Cards not found") {
                every { cardRepository.find(any(), any(), any()) } returns emptyList()

                it("Should return empty when card not found") {
                    cardService.findCards(emptySet(), emptySet(), emptySet()) shouldBe emptyList()

                    verify { cardRepository.find(eq(emptySet()), eq(emptySet()), eq(emptySet())) }
                    confirmVerified(cardRepository)
                }
            }

            context("Cards found") {
                every { cardRepository.find(any(), any(), any()) } returns listOf(CardServiceTestFixture.card)

                it("Should return card") {
                    cardService.findCards(emptySet(), emptySet(), emptySet()) shouldBe listOf(CardServiceTestFixture.cardQuery)

                    verify { cardRepository.find(eq(emptySet()), eq(emptySet()), eq(emptySet())) }
                    confirmVerified(cardRepository)
                }
            }
        }

        describe("Find card by cardId") {

            context("Card not found") {
                every { cardRepository.findByCardId(any()) } returns null

                it("Should return null when card not found") {
                    cardService.findCardByCardId(CardServiceTestFixture.cardId) shouldBe null

                    verify { cardRepository.findByCardId(eq(CardServiceTestFixture.cardId)) }
                    confirmVerified(cardRepository)
                }
            }

            context("Card found") {
                every { cardRepository.findByCardId(any()) } returns CardServiceTestFixture.card

                it("Should return card") {
                    cardService.findCardByCardId(CardServiceTestFixture.cardId) shouldBe CardServiceTestFixture.cardQuery

                    verify { cardRepository.findByCardId(eq(CardServiceTestFixture.cardId)) }
                    confirmVerified(cardRepository)
                }
            }
        }
    }
}