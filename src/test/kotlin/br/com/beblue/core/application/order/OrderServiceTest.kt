package br.com.beblue.core.application.order

import br.com.beblue.containViolation
import br.com.beblue.core.application.order.command.OrderCreationCommand
import br.com.beblue.core.domain.card.CardRepository
import br.com.beblue.core.domain.order.Item
import br.com.beblue.core.domain.order.OrderRepository
import br.com.beblue.core.domain.player.PlayerRepository
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.DescribeSpec
import io.mockk.clearMocks
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.valiktor.ConstraintViolationException
import org.valiktor.constraints.Valid

class OrderServiceTest : DescribeSpec() {

    private val orderRepository = mockk<OrderRepository>()
    private val playerRepository = mockk<PlayerRepository>()
    private val cardRepository = mockk<CardRepository>()
    private val orderService = OrderService(orderRepository, playerRepository, cardRepository)

    override fun afterTest(testCase: TestCase, result: TestResult) = clearMocks(orderRepository, playerRepository, cardRepository)

    init {
        describe("Find orders by playerId") {

            context("Orders not found") {
                every { orderRepository.findByPlayerId(any()) } returns emptyList()

                it("Should return empty when order not found") {
                    orderService.findOrdersByPlayerId(OrderServiceTestFixture.playerId) shouldBe emptyList()

                    verify { orderRepository.findByPlayerId(eq(OrderServiceTestFixture.playerId)) }
                    confirmVerified(orderRepository)
                }
            }

            context("Orders found") {
                every { orderRepository.findByPlayerId(any()) } returns listOf(OrderServiceTestFixture.order)
                OrderServiceTestFixture.order.items.map(Item::cardId).forEach {
                    every { cardRepository.findByCardId(eq(it)) } returns OrderServiceTestFixture.cards[it]
                }

                it("Should return orders") {
                    orderService.findOrdersByPlayerId(OrderServiceTestFixture.playerId) shouldBe listOf(OrderServiceTestFixture.orderQuery)

                    verify { orderRepository.findByPlayerId(eq(OrderServiceTestFixture.playerId)) }
                    OrderServiceTestFixture.order.items.map(Item::cardId).forEach {
                        verify { cardRepository.findByCardId(it) }
                    }
                    confirmVerified(orderRepository, cardRepository)
                }
            }
        }

        describe("Find order by orderId") {

            context("Order not found") {
                every { orderRepository.findByOrderId(any()) } returns null

                it("Should return null when order not found") {
                    orderService.findOrderByOrderId(OrderServiceTestFixture.orderId) shouldBe null

                    verify { orderRepository.findByOrderId(eq(OrderServiceTestFixture.orderId)) }
                    confirmVerified(orderRepository)
                }
            }

            context("Order found") {
                every { orderRepository.findByOrderId(any()) } returns OrderServiceTestFixture.order
                OrderServiceTestFixture.order.items.map(Item::cardId).forEach {
                    every { cardRepository.findByCardId(eq(it)) } returns OrderServiceTestFixture.cards[it]
                }

                it("Should return order") {
                    orderService.findOrderByOrderId(OrderServiceTestFixture.orderId) shouldBe OrderServiceTestFixture.orderQuery

                    verify { orderRepository.findByOrderId(eq(OrderServiceTestFixture.orderId)) }
                    OrderServiceTestFixture.order.items.map(Item::cardId).forEach {
                        verify { cardRepository.findByCardId(it) }
                    }
                    confirmVerified(orderRepository, cardRepository)
                }
            }
        }

        describe("Create new order") {
            context("Player does not exist") {
                every { playerRepository.findByPlayerId(any()) } returns null
                OrderServiceTestFixture.orderCreationCommand.items.map(OrderCreationCommand.Item::cardId).forEach {
                    every { cardRepository.findByCardId(eq(it)) } returns OrderServiceTestFixture.cards[it]
                }

                it("Should fail when player does not exist") {
                    val ex = shouldThrow<ConstraintViolationException> {
                        orderService.createNewOrder(OrderServiceTestFixture.orderCreationCommand)
                    }

                    ex should containViolation("playerId", OrderServiceTestFixture.playerId, Valid)

                    verify { playerRepository.findByPlayerId(eq(OrderServiceTestFixture.playerId)) }
                    OrderServiceTestFixture.orderCreationCommand.items.map(OrderCreationCommand.Item::cardId).forEach {
                        verify { cardRepository.findByCardId(it) }
                    }
                    confirmVerified(playerRepository, cardRepository)
                }
            }

            context("Card does not exist") {
                every { playerRepository.findByPlayerId(any()) } returns OrderServiceTestFixture.player
                every { cardRepository.findByCardId(any()) } returns null

                it("Should fail when card does not exist") {
                    val ex = shouldThrow<ConstraintViolationException> {
                        orderService.createNewOrder(OrderServiceTestFixture.orderCreationCommand)
                    }

                    ex should containViolation("items[0].cardId", "base1-1", Valid)
                    ex should containViolation("items[1].cardId", "base1-10", Valid)
                    ex should containViolation("items[2].cardId", "base1-20", Valid)

                    verify { playerRepository.findByPlayerId(eq(OrderServiceTestFixture.playerId)) }
                    OrderServiceTestFixture.orderCreationCommand.items.map(OrderCreationCommand.Item::cardId).forEach {
                        verify { cardRepository.findByCardId(it) }
                    }
                    confirmVerified(playerRepository, cardRepository)
                }
            }

            context("Player and cards exist") {
                every { playerRepository.findByPlayerId(any()) } returns OrderServiceTestFixture.player
                OrderServiceTestFixture.orderCreationCommand.items.map(OrderCreationCommand.Item::cardId).forEach {
                    every { cardRepository.findByCardId(eq(it)) } returns OrderServiceTestFixture.cards[it]
                }
                every { orderRepository.create(any()) } returns Unit
                every { playerRepository.update(any()) } returns Unit

                it("Should create new order") {
                    val orderId = orderService.createNewOrder(OrderServiceTestFixture.orderCreationCommand)
                    orderId shouldNotBe null

                    verify { playerRepository.findByPlayerId(eq(OrderServiceTestFixture.playerId)) }
                    OrderServiceTestFixture.orderCreationCommand.items.map(OrderCreationCommand.Item::cardId).forEach {
                        verify { cardRepository.findByCardId(it) }
                    }
                    verify { orderRepository.create(eq(OrderServiceTestFixture.order.copy(orderId = orderId))) }
                    verify { playerRepository.update(eq(OrderServiceTestFixture.playerAfterPayment)) }
                    confirmVerified(playerRepository, cardRepository, orderRepository)
                }
            }
        }
    }
}