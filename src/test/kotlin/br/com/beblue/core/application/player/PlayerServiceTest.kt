package br.com.beblue.core.application.player

import br.com.beblue.core.domain.card.CardRepository
import br.com.beblue.core.domain.player.PlayerAlreadyExistsException
import br.com.beblue.core.domain.player.PlayerRepository
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.DescribeSpec
import io.mockk.clearMocks
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify

class PlayerServiceTest : DescribeSpec() {

    private val playerRepository = mockk<PlayerRepository>()
    private val cardRepository = mockk<CardRepository>()
    private val playerService = PlayerService(playerRepository, cardRepository)

    override fun afterTest(testCase: TestCase, result: TestResult) = clearMocks(playerRepository, cardRepository)

    init {
        describe("Find player by playerId") {

            context("Player not found") {
                every { playerRepository.findByPlayerId(any()) } returns null

                it("Should return null when player not found") {
                    playerService.findPlayerByPlayerId(PlayerServiceTestFixture.playerId) shouldBe null

                    verify { playerRepository.findByPlayerId(eq(PlayerServiceTestFixture.playerId)) }
                    confirmVerified(playerRepository, cardRepository)
                }
            }

            context("Player found") {
                every { playerRepository.findByPlayerId(any()) } returns PlayerServiceTestFixture.player

                it("Should return player") {
                    playerService.findPlayerByPlayerId(PlayerServiceTestFixture.playerId) shouldBe PlayerServiceTestFixture.playerQuery

                    verify { playerRepository.findByPlayerId(eq(PlayerServiceTestFixture.playerId)) }
                    confirmVerified(playerRepository, cardRepository)
                }
            }
        }

        describe("Register player") {
            context("Player already exists") {
                every { playerRepository.findByPlayerId(any()) } returns PlayerServiceTestFixture.player

                it("Should fail when player already exists") {
                    val ex = shouldThrow<PlayerAlreadyExistsException> {
                        playerService.registerPlayer(PlayerServiceTestFixture.playerRegistrationCommand)
                    }

                    ex.playerId shouldBe PlayerServiceTestFixture.playerId
                    verify { playerRepository.findByPlayerId(eq(PlayerServiceTestFixture.playerId)) }
                    confirmVerified(playerRepository)
                }
            }

            context("Player does not exist") {
                every { playerRepository.findByPlayerId(any()) } returns null
                every { playerRepository.create(any()) } returns Unit

                it("Should register player") {
                    playerService.registerPlayer(PlayerServiceTestFixture.playerRegistrationCommand)

                    verify { playerRepository.findByPlayerId(eq(PlayerServiceTestFixture.playerId)) }
                    verify { playerRepository.create(eq(PlayerServiceTestFixture.player)) }
                    confirmVerified(playerRepository)
                }
            }
        }
    }
}