package br.com.beblue.core.application.player

import br.com.beblue.core.application.player.command.PlayerRegistrationCommand
import br.com.beblue.core.application.player.query.PlayerQuery
import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.player.Player
import br.com.beblue.core.domain.player.PlayerId
import br.com.beblue.core.domain.toMoney

object PlayerServiceTestFixture {

    const val playerId: PlayerId = "50732984033"

    val player = Player(
        playerId = playerId,
        name = "Player 1"
    )

    val playerQuery = PlayerQuery(
        playerId = playerId,
        name = "Player 1",
        balance = PlayerQuery.Balance(
            availableAmount = 100.toMoney(BRL),
            availableCashbackAmount = 0.toMoney(BRL)
        ),
        deck = emptySet()
    )

    val playerRegistrationCommand = PlayerRegistrationCommand(
        playerId = playerId,
        name = "Player 1"
    )
}