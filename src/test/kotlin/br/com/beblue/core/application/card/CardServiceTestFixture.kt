package br.com.beblue.core.application.card

import br.com.beblue.core.application.card.query.CardQuery
import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.card.Ability
import br.com.beblue.core.domain.card.Attack
import br.com.beblue.core.domain.card.Card
import br.com.beblue.core.domain.card.CardId
import br.com.beblue.core.domain.card.Rarity
import br.com.beblue.core.domain.card.Subtype
import br.com.beblue.core.domain.card.Type
import br.com.beblue.core.domain.toMoney
import java.net.URI

object CardServiceTestFixture {

    const val cardId: CardId = "base1-1"

    val card = Card(
        cardId = cardId,
        name = "Alakazam",
        number = 1,
        hp = 80,
        rarity = Rarity.RARE,
        type = Type.PSYCHIC,
        subtype = Subtype.STAGE_2,
        evolvesFrom = "Kadabra",
        ability = Ability(
            name = "Damage Swap",
            text = "As often as you like during your turn (before your attack), you may move 1 damage counter from 1 of your Pokémon to another as long as you don't Knock Out that Pokémon. This power can't be used if Alakazam is Asleep, Confused, or Paralyzed."
        ),
        attacks = setOf(
            Attack(
                name = "Confuse Ray",
                text = "Flip a coin. If heads, defender is now Confused.",
                damage = "30",
                cost = setOf(Type.PSYCHIC)
            )
        ),
        resistances = emptyMap(),
        weaknesses = mapOf(
            Type.PSYCHIC to "×2"
        ),
        smallImageUri = URI.create("https://images.pokemontcg.io/base1/1.png"),
        largeImageUri = URI.create("https://images.pokemontcg.io/base1/1_hires.png")
    )

    val cardQuery = CardQuery(
        cardId = cardId,
        name = "Alakazam",
        number = 1,
        hp = 80,
        rarity = "Rare",
        type = "Psychic",
        subtype = "Stage 2",
        evolvesFrom = "Kadabra",
        ability = CardQuery.Ability(
            name = "Damage Swap",
            text = "As often as you like during your turn (before your attack), you may move 1 damage counter from 1 of your Pokémon to another as long as you don't Knock Out that Pokémon. This power can't be used if Alakazam is Asleep, Confused, or Paralyzed."
        ),
        attacks = setOf(
            CardQuery.Attack(
                name = "Confuse Ray",
                text = "Flip a coin. If heads, defender is now Confused.",
                damage = "30",
                cost = setOf("Psychic")
            )
        ),
        resistances = emptySet(),
        weaknesses = setOf(
            CardQuery.Weakness(
                type = "Psychic",
                value = "×2"
            )
        ),
        cashbackPercentage = 20,
        amount = 10.4.toMoney(BRL),
        smallImageUri = "https://images.pokemontcg.io/base1/1.png",
        largeImageUri = "https://images.pokemontcg.io/base1/1_hires.png"
    )
}