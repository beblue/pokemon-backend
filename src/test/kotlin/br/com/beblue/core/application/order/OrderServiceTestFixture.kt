package br.com.beblue.core.application.order

import br.com.beblue.core.application.order.command.OrderCreationCommand
import br.com.beblue.core.application.order.query.OrderQuery
import br.com.beblue.core.domain.BRL
import br.com.beblue.core.domain.card.Ability
import br.com.beblue.core.domain.card.Attack
import br.com.beblue.core.domain.card.Card
import br.com.beblue.core.domain.card.Rarity
import br.com.beblue.core.domain.card.Subtype
import br.com.beblue.core.domain.card.Type
import br.com.beblue.core.domain.order.Item
import br.com.beblue.core.domain.order.Order
import br.com.beblue.core.domain.order.OrderId
import br.com.beblue.core.domain.order.OrderStatus
import br.com.beblue.core.domain.player.Balance
import br.com.beblue.core.domain.player.Player
import br.com.beblue.core.domain.player.PlayerId
import br.com.beblue.core.domain.toMoney
import java.net.URI

object OrderServiceTestFixture {

    const val playerId: PlayerId = "50732984033"

    val player = Player(
        playerId = playerId,
        name = "Player 1",
        balance = Balance(
            availableAmount = 100.toMoney(BRL),
            availableCashbackAmount = 0.toMoney(BRL)
        ),
        deck = emptySet()
    )

    val playerAfterPayment = Player(
        playerId = playerId,
        name = "Player 1",
        balance = Balance(
            availableAmount = 77.9.toMoney(BRL),
            availableCashbackAmount = 5.36.toMoney(BRL)
        ),
        deck = setOf(
            "base1-1",
            "base1-10",
            "base1-20"
        )
    )

    val cards = mapOf(
        "base1-1" to Card(
            cardId = "base1-1",
            name = "Alakazam",
            number = 1,
            hp = 80,
            rarity = Rarity.RARE,
            type = Type.PSYCHIC,
            subtype = Subtype.STAGE_2,
            evolvesFrom = "Kadabra",
            ability = Ability(
                name = "Damage Swap",
                text = "As often as you like during your turn (before your attack), you may move 1 damage counter from 1 of your Pokémon to another as long as you don't Knock Out that Pokémon. This power can't be used if Alakazam is Asleep, Confused, or Paralyzed."
            ),
            attacks = setOf(
                Attack(
                    name = "Confuse Ray",
                    text = "Flip a coin. If heads, defender is now Confused.",
                    damage = "30",
                    cost = setOf(Type.PSYCHIC)
                )
            ),
            resistances = emptyMap(),
            weaknesses = mapOf(
                Type.PSYCHIC to "×2"
            ),
            smallImageUri = URI.create("https://images.pokemontcg.io/base1/1.png"),
            largeImageUri = URI.create("https://images.pokemontcg.io/base1/1_hires.png")
        ),
        "base1-10" to Card(
            cardId = "base1-10",
            name = "Mewtwo",
            number = 10,
            hp = 60,
            rarity = Rarity.RARE,
            type = Type.PSYCHIC,
            subtype = Subtype.BASIC,
            evolvesFrom = null,
            ability = null,
            attacks = setOf(
                Attack(
                    name = "Psychic",
                    text = "Does 10 damage plus 10 more damage for each Energy card attached to the Defending Pokémon.",
                    damage = "10+",
                    cost = setOf(
                        Type.PSYCHIC,
                        Type.COLORLESS
                    )
                ),
                Attack(
                    name = "Barrier",
                    text = "Discard 1 Energy card attached to Mewtwo in order to use this attack. During your opponent's next turn, prevent all effects of attacks, including damage, done to Mewtwo.",
                    damage = "",
                    cost = setOf(Type.PSYCHIC)
                )
            ),
            resistances = emptyMap(),
            weaknesses = mapOf(
                Type.PSYCHIC to "×2"
            ),
            smallImageUri = URI.create("https://images.pokemontcg.io/base1/10.png"),
            largeImageUri = URI.create("https://images.pokemontcg.io/base1/10_hires.png")
        ),
        "base1-20" to Card(
            cardId = "base1-20",
            name = "Electabuzz",
            number = 20,
            hp = 70,
            rarity = Rarity.RARE,
            type = Type.LIGHTNING,
            subtype = Subtype.BASIC,
            evolvesFrom = null,
            ability = null,
            attacks = setOf(
                Attack(
                    name = "Thundershock",
                    text = "Flip a coin. If heads, the Defending Pokémon is now Paralyzed.",
                    damage = "10",
                    cost = setOf(Type.LIGHTNING)
                ),
                Attack(
                    name = "Thunderpunch",
                    text = "Flip a coin. If heads, this attack does 30 damage plus 10 more damage; if tails, this attack does 30 damage and Electabuzz does 10 damage to itself.",
                    damage = "30+",
                    cost = setOf(
                        Type.COLORLESS,
                        Type.LIGHTNING
                    )
                )
            ),
            resistances = emptyMap(),
            weaknesses = mapOf(
                Type.FIGHTING to "×2"
            ),
            smallImageUri = URI.create("https://images.pokemontcg.io/base1/20.png"),
            largeImageUri = URI.create("https://images.pokemontcg.io/base1/20_hires.png")
        )
    )

    val orderId: OrderId = OrderId.randomUUID()

    val order = Order(
        orderId = orderId,
        playerId = playerId,
        status = OrderStatus.CONFIRMED,
        items = setOf(
            Item(
                cardId = "base1-1",
                cashbackPercentage = 20,
                amount = 10.4.toMoney(BRL),
                paidAmount = 10.4.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 2.08.toMoney(BRL)
            ),
            Item(
                cardId = "base1-10",
                cashbackPercentage = 20,
                amount = 5.4.toMoney(BRL),
                paidAmount = 5.4.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 1.08.toMoney(BRL)
            ),
            Item(
                cardId = "base1-20",
                cashbackPercentage = 35,
                amount = 6.3.toMoney(BRL),
                paidAmount = 6.3.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 2.2.toMoney(BRL)
            )
        )
    )

    val orderQuery = OrderQuery(
        orderId = order.orderId,
        playerId = playerId,
        occurredOn = order.occurredOn,
        totalAmount = 22.1.toMoney(BRL),
        totalPaidAmount = 22.1.toMoney(BRL),
        totalBurnedCashbackAmount = 0.toMoney(BRL),
        totalEarnedCashbackAmount = 5.36.toMoney(BRL),
        items = setOf(
            OrderQuery.Item(
                card = OrderQuery.Item.Card(
                    cardId = "base1-1",
                    name = "Alakazam",
                    number = 1,
                    smallImageUri = "https://images.pokemontcg.io/base1/1.png",
                    largeImageUri = "https://images.pokemontcg.io/base1/1_hires.png"
                ),
                cashbackPercentage = 20,
                amount = 10.4.toMoney(BRL),
                paidAmount = 10.4.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 2.08.toMoney(BRL)
            ),
            OrderQuery.Item(
                card = OrderQuery.Item.Card(
                    cardId = "base1-10",
                    name = "Mewtwo",
                    number = 10,
                    smallImageUri = "https://images.pokemontcg.io/base1/10.png",
                    largeImageUri = "https://images.pokemontcg.io/base1/10_hires.png"
                ),
                cashbackPercentage = 20,
                amount = 5.4.toMoney(BRL),
                paidAmount = 5.4.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 1.08.toMoney(BRL)
            ),
            OrderQuery.Item(
                card = OrderQuery.Item.Card(
                    cardId = "base1-20",
                    name = "Electabuzz",
                    number = 20,
                    smallImageUri = "https://images.pokemontcg.io/base1/20.png",
                    largeImageUri = "https://images.pokemontcg.io/base1/20_hires.png"
                ),
                cashbackPercentage = 35,
                amount = 6.3.toMoney(BRL),
                paidAmount = 6.3.toMoney(BRL),
                burnedCashbackAmount = 0.toMoney(BRL),
                earnedCashbackAmount = 2.2.toMoney(BRL)
            )
        )
    )

    val orderCreationCommand = OrderCreationCommand(
        playerId = playerId,
        items = setOf(
            OrderCreationCommand.Item(cardId = "base1-1"),
            OrderCreationCommand.Item(cardId = "base1-10"),
            OrderCreationCommand.Item(cardId = "base1-20")
        )
    )
}