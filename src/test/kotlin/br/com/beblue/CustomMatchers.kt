package br.com.beblue

import io.kotlintest.Matcher
import io.kotlintest.Result
import org.valiktor.Constraint
import org.valiktor.ConstraintViolationException

fun containViolation(property: String, invalidValue: Any?, constraint: Constraint) = object : Matcher<ConstraintViolationException> {

    override fun test(value: ConstraintViolationException) = Result(
        passed = value.constraintViolations.any { it.property == property && it.value == invalidValue && it.constraint == constraint },
        failureMessage = "Should contain 1 violation with property $property, value $invalidValue and constraint $constraint",
        negatedFailureMessage = "Should not contain 1 violation with property $property, value $invalidValue and constraint $constraint"
    )
}