FROM openjdk:8-jre-alpine
MAINTAINER br.com.beblue
RUN apk update \
  && apk add curl jq tzdata fontconfig ttf-dejavu \
  && cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
  && echo 'America/Sao_Paulo' > /etc/timezone \
  && apk del tzdata
RUN mkdir -p /app
WORKDIR /app
COPY build/libs/app.jar /app
ENTRYPOINT exec java $JAVA_OPTIONS -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -XshowSettings:vm -jar app.jar
CMD [""]